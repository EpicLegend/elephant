$(document).ready(function () {
	// slick slider
	$(".hot-slider").each(function (index, item){
		$(item).slick({
			lazyLoad: 'ondemand',
			dots: false,
			infinite: true,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 4000,
			speed: 1000,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
						arrows: true,
						centerMode: true,
						focusOnSelect: true,
						centerPadding: "30px"
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						arrows: true,
						centerMode: true,
						focusOnSelect: true,
						centerPadding: "30px"
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: true,
						centerMode: true,
						focusOnSelect: true,
						centerPadding: "30px"
					}
				}
			]
		});
	});

	// slick slider
	$(".blog-slider").each(function (index, item){
		$(item).slick({
			lazyLoad: 'ondemand',
			dots: false,
			infinite: true,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 4000,
			speed: 1000,
			slidesToShow: 3,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
						arrows: true,
						centerMode: true,
						focusOnSelect: true,
						centerPadding: "30px"
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						arrows: true,
						centerMode: true,
						focusOnSelect: true,
						centerPadding: "30px"
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: true,
						centerMode: true,
						focusOnSelect: true,
						centerPadding: "30px"
					}
				}
			]
		});
	});


});
