//= ../node_modules/lazysizes/lazysizes.js
//= ../node_modules/jquery/dist/jquery.js
//= ../node_modules/popper.js/dist/umd/popper.js
//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js
//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js

//= library/slick.js
//= library/jquery-ui.js
//= components/sliders.js

$(document).ready(function () {



	// mini cart
	$(".mini-cart_btn").on("click", function (e){
		e.stopPropagation();
		e.preventDefault();
		$("#mini-cart-modal").addClass("show");
		$("body").css("overflow", "hidden");
	});
	$("#mini-cart-modal").on("click", function (e) {
		$(this).removeClass("show");
		$("body").css("overflow", "auto");
	});
	$("#mini-cart-modal .mini-cart-modal__close").on("click", function (e) {
		$("#mini-cart-modal").removeClass("show");
		$("body").css("overflow", "auto");
	});
	$("#mini-cart-modal .mini-cart-modal__content").on("click", function(e) {
		e.stopPropagation();
		e.preventDefault();
	});

	// menu
	$(".burger").on("click", function (e){
		$("#mobile-menu-modal").addClass("show");
		$("body").css("overflow", "hidden");
	});
	$("#mobile-menu-modal").on("click", function (e) {
		$(this).removeClass("show");
		$("body").css("overflow", "auto");
	});
	$(".header-mobile__close").on("click", function (e) {
		$("#mobile-menu-modal").removeClass("show");
		$("body").css("overflow", "auto");
	});
	$("#mobile-menu-modal .header-mobile__content").on("click", function(e) {
		e.stopPropagation();
		e.preventDefault();
	});
	$(window).resize(function(){
		if($(window).width() >= 991) {
			$("#mobile-menu-modal").removeClass("show");
			$("#mini-cart-modal").removeClass("show");
		}
	});

	// search form
	$(".search-btn").on("click", function (e){
		e.stopPropagation();
		e.preventDefault();

		$(this).parent().find(".search-form").addClass("show");
	});
	$(".search-form__close").on("click", function (e){
		e.stopPropagation();
		e.preventDefault();

		$(this).parent().parent().parent().find(".search-form").removeClass("show");
	});






	// slider range price
	$( "#price-range" ).slider({
		range: true,
		min: 0,
		max: 500,
		values: [ 75, 300 ],
		slide: function( event, ui ) {
			$( "#price-range__min" ).val( ui.values[ 0 ] );
			$( "#price-range__max" ).val( ui.values[ 1 ] );
		}
	});
	$( "#price-range__min" ).val( $("#price-range").slider( "values", 0 ) );
	$( "#price-range__max" ).val( $("#price-range").slider( "values", 1 ) );
	$( "#price-range__min, #price-range__max" ).on( "change", function() {
		$("#price-range").slider( "option", "values", [ parseInt( $("#price-range__min").val() ) , parseInt( $("#price-range__max").val() ) ] );
	});



	// catalog-options__dots
	// меняем сетку в каталоге
	$(".catalog-options__dots").on("click", function(e){
		$(".catalog-options__dots").removeClass("active");
		$(this).addClass("active");

		$("#catalog-grid").removeClass("catalog__row_2");
		$("#catalog-grid").removeClass("catalog__row_3");
		$("#catalog-grid").removeClass("catalog__row_4");

		$("#catalog-grid").addClass("catalog__row_" + $(this).attr("data-grid"));
		// console.log($(this).attr("data-grid"));
	});

	// Фильтр в телефоне
	$(".filter-mob").on("click", function (e) {
		$("#catalog-sidebar-mob").addClass("show");
		$("body").css("overflow", "hidden");
	});
	$("#catalog-sidebar-mob").on("click", function (e) {
		$(this).removeClass("show");
		$("body").css("overflow", "auto");
	});
	$(".catalog-sidebar-mob__close").on("click", function (e) {
		$("#catalog-sidebar-mob").removeClass("show");
		$("body").css("overflow", "auto");
	});
	$("#catalog-sidebar-mob .content").on("click", function(e) {
		e.stopPropagation();
		e.preventDefault();
	});
	$(window).resize(function(e){
		mobFilter();
	});
	mobFilter();
	function mobFilter() {
		if( $(window).width() > 991 ) {
			$("#catalog-sidebar-wrapper").append(  $("#catalog-sidebar") );
			$("#catalog-sidebar-mob").removeClass("show");
		} else {
			$("#catalog-sidebar-mob .content").append( $("#catalog-sidebar") );
		}
	}


	// slider welcome
	$(".welcome-slider").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		speed: 500,
		fade: true,
		autoplay: true,
		autoplaySpeed: 8000,
		arrows: false,
		dots: false,
	});

	// slider item
	$('#product-for').slick({
		lazyLoad: 'ondemand',
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '#product-nav'

	});
	$('#product-nav').slick({
		lazyLoad: 'ondemand',
		vertical: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: '#product-for',
		dots: false,
		centerMode: true,
		prevArrow: '<div class="slick-arrow_prev"><svg version="1.1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px" viewBox="0 0 443.52 443.52" style="enable-background:new 0 0 443.52 443.52;" xml:space="preserve"><path d="M336.226,209.591l-204.8-204.8c-6.78-6.548-17.584-6.36-24.132,0.42c-6.388,6.614-6.388,17.099,0,23.712l192.734,192.734 L107.294,414.391c-6.663,6.664-6.663,17.468,0,24.132c6.665,6.663,17.468,6.663,24.132,0l204.8-204.8 C342.889,227.058,342.889,216.255,336.226,209.591z"/></svg></div>',
		nextArrow: '<div class="slick-arrow_next"><svg version="1.1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px" viewBox="0 0 443.52 443.52" style="enable-background:new 0 0 443.52 443.52;" xml:space="preserve"><path d="M336.226,209.591l-204.8-204.8c-6.78-6.548-17.584-6.36-24.132,0.42c-6.388,6.614-6.388,17.099,0,23.712l192.734,192.734 L107.294,414.391c-6.663,6.664-6.663,17.468,0,24.132c6.665,6.663,17.468,6.663,24.132,0l204.8-204.8 C342.889,227.058,342.889,216.255,336.226,209.591z"/></svg></div>',
		focusOnSelect: true,
		responsive: [
			{
			breakpoint: 991,
			settings: {
				vertical: false,
				}
			},
			{
				breakpoint: 700,
				settings: {
					slidesToShow: 3,
					vertical: false,
					slidesToScroll: 1
				}
			}
		]
	});

	// input number
	// $(".input_number .min").on("click", function(e){
	// 	var result = $(this).parent().find("input[type='number']").val();
	// 	if ( result >= 2) {
	// 		result--;
	// 	}
	// 	$(this).parent().find("input[type='number']").val( result );
	// 	console.log( $(this).parent().find("input[type='number']").val() );
	// });
	// $(".input_number .plus").on("click", function(e){
	// 	var result = $(this).parent().find("input[type='number']").val();
	// 	result++;
	// 	$(this).parent().find("input[type='number']").val( result );
	// });
	// $(".input_number input[type='number']").change(function(e){
	// 	console.log(1111);
	// });
	// $(".input_number input[type='number']").on("click", function(e){
	// 	console.log(2222);
	// });
	$('.input_number span').click(function(){
		if($(this).is('.plus')){
			$(this).siblings("input[type='number']")[0].stepUp();
		}else{
			$(this).siblings("input[type='number']")[0].stepDown();
		}
	})
});
